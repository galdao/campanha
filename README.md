# Desafio Campanha #

Esse repositório contêm um demo de uma API para o gerenciamento de campanhas usando Spring Boot, Spring Framework, Spring Data, Hibernate Validator, um MongoDB e um Message Broker aninhados.

### Como usar ###

* Clonar o repositório
* No diretório do repositório rodar o comando: *mvn clean package*
* Entrar no diretório target
* Rodar o comando: *java -jar campanha.jar*

### Endpoints ###

* **GET**  http://localhost:10100/campanha - Retorna todas as campanhas registradas
* **GET**  http://localhost:10100/campanha/{id} - Retorna campanha com id desejado
* **POST**  http://localhost:10100/campanha - Registra uma nova campanha
* **PUT**  http://localhost:10100/campanha/{id} - Atualiza uma campanha existente
* **DELETE**  http://localhost:10100/campanha/{id} - Deleta uma campanha existente

Corpo da request para requests POST, PUT: {"nome":"Super Campanha","time":"Corinthians","inicio":"2017-07-13", "fim":"2017-07-20"}


### Requerimentos ###

* Java 8
* Maven