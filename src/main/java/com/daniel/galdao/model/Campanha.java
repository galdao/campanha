package com.daniel.galdao.model;

import com.daniel.galdao.validation.DateRangeCheck;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */
@DateRangeCheck(startDateField = "startDate", endDateField = "endDate")
public class Campanha {

    @Id
    private String id;

    @NotNull
    @NotEmpty
    @JsonProperty("nome")
    private String name;
    @NotNull
    @NotEmpty
    @JsonProperty("time")
    private String team;
    @NotNull
    @JsonProperty("inicio")
    private LocalDate startDate;
    @NotNull
    @JsonProperty("fim")
    private LocalDate endDate;

    @JsonProperty
    public String getId() {
        return id;
    }

    @JsonIgnore //Ignorar campo caso o usuario tente envialo em seu input, mas ainda devolver seu valor nas respostas
    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public void addDayToCampanha(){
        this.endDate = endDate.plusDays(1);
    }

}
