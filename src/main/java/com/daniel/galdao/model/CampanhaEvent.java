package com.daniel.galdao.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
public class CampanhaEvent {

    @Id
    @JsonIgnore
    private String id;

    private String type;
    private Object data;

    public CampanhaEvent() {
    }

    public CampanhaEvent(String type, Object data) {
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
