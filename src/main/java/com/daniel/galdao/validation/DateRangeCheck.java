package com.daniel.galdao.validation;

import javax.validation.Payload;

/**
 * Created by IFC.Daniel on 13/07/2017.
 */
//Annotation usada para validacao do Hibernate Validator/Javax validation
public @interface DateRangeCheck {

    String message() default "data de inicio deve ser antes da data de final";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };

    String startDateField();

    String endDateField();

}