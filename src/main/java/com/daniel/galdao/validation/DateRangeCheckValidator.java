package com.daniel.galdao.validation;

import org.apache.commons.beanutils.PropertyUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

/**
 * Created by IFC.Daniel on 13/07/2017.
 */
public class DateRangeCheckValidator implements ConstraintValidator<DateRangeCheck, Object> {

    private String startDateField;
    private String endDateField;

    @Override
    public void initialize(DateRangeCheck constraintAnnotation) {
        this.startDateField = constraintAnnotation.startDateField();
        this.endDateField = constraintAnnotation.endDateField();
    }

    //Valida se a startDate é antes da endDate do objeto anotado.
    //Retorna valido se algum dos 2 campos for null, pq idealmente isso é válidado em outro validador.
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        LocalDate startDate = null;
        LocalDate endDate = null;
        try {
            startDate = (LocalDate) PropertyUtils.getSimpleProperty(value, this.startDateField);
            endDate = (LocalDate) PropertyUtils.getSimpleProperty(value, this.endDateField);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return startDate == null || endDate == null || !startDate.equals(endDate) || startDate.isBefore(endDate);
    }

}
