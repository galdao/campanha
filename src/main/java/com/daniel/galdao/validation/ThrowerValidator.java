package com.daniel.galdao.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */
public class ThrowerValidator implements Validator {

    private javax.validation.Validator validator;

    public ThrowerValidator() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    //Valida um objeto anotado com @Valid no parametro de um metodo do controller
    //Usando um validador do Javax Validation e jogando uma IllegalArgumentException caso haja erro.
    @Override
    public void validate(Object target, Errors errors) {
        Object[] violations = validator.validate(target).toArray();
        if (violations.length == 0) {
            return;
        }
        String errorMessage = Arrays.stream(violations)
                .map(v -> (ConstraintViolation<?>) v)
                .map(cv -> String.format("%s %s", cv.getPropertyPath(), cv.getMessage()))
                .collect(Collectors.joining("; "));
        throw new IllegalArgumentException(errorMessage);
    }
}