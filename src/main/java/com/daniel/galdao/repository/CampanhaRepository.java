package com.daniel.galdao.repository;

import com.daniel.galdao.model.Campanha;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */
public interface CampanhaRepository extends MongoRepository<Campanha, String> {


    List<Campanha> findByEndDateAfter(LocalDate date);
    List<Campanha> findByTeamAndEndDateAfter(String team, LocalDate date);

    //Query para buscar campanha com data que intersecta com data dada
    //Lógica dela é que existem 2 situacoes para testar se um periodo intersecta:
    //1 - Periodo1 comeca antes do comeco de Periodo2 e Periodo1 termina depois do comeco de Periodo2
    //2 - Periodo1 comeca depois do comeco de Periodo2 e Periodo1 comeca antes do termino de Periodo2
    @Query("{$or:[{$and:[{'startDate':{$lte:?0}},{'endDate':{$gte:?0}}]},{$and:[{'startDate':{$gte:?0}},{'startDate':{$lte:?1}}]}]}")
    List<Campanha> findIntersectingCampanhhas(LocalDate startDate, LocalDate endDate);

}
