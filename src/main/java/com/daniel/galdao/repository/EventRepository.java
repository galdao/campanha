package com.daniel.galdao.repository;

import com.daniel.galdao.model.CampanhaEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
public interface EventRepository extends MongoRepository<CampanhaEvent, String> {

}
