package com.daniel.galdao.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */

@Primary
@Component
public class CustomObjectMapper extends ObjectMapper {

    public CustomObjectMapper() {
        //Necessário registrar o módulo do javatime no jackson para serializar os campos LocalDate corretamente.
        registerModule(new JavaTimeModule());
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

}
