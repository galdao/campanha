package com.daniel.galdao.config;

import com.daniel.galdao.model.ErrorResponse;
import com.daniel.galdao.validation.ThrowerValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */
@RestControllerAdvice
public class GlobalRestControllerAdvice {

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        //Faz com que a anotação @Valid do spring nos controllers utilize meu validador customizado que valida com o Hibernate Validator
        // e joga uma exception no formato desejado.
        binder.setValidator(new ThrowerValidator());
    }

    @ExceptionHandler({IllegalArgumentException.class, HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ErrorResponse handleBadRequestException(Exception ex) {
        //Usado para retornar mensagem de erro quando uma request é invalida
        return new ErrorResponse(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected ErrorResponse handleException(Exception ex) {
        //Usado para retornar uma mensagem de erro padrao quando ocorre uma exception inesperada
        return new ErrorResponse("Occoreu um erro!");
    }

}
