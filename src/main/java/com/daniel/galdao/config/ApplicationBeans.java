package com.daniel.galdao.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
@Configuration
public class ApplicationBeans {

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

}
