package com.daniel.galdao.service.embedded;

import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version.Main;
import de.flapdoodle.embed.process.runtime.Network;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */
@Service
public class EmbeddedMongoService {

    private static final String MONGO_HOST = "localhost";
    private static final int MONGO_PORT = 10150;

    private MongodExecutable mongodExecutable;

    /*
     * Um serviço de mongoDB aninhado.
     * Normalmente ele só é utilizado para testes.
     */
    @PostConstruct
    protected void startEmbeddedMongoDb() throws Exception {
        MongodStarter mongodStarter = MongodStarter.getDefaultInstance();
        IMongodConfig iMongoConfig = new MongodConfigBuilder()
                .version(Main.PRODUCTION)
                .net(new Net(MONGO_HOST, MONGO_PORT, Network.localhostIsIPv6()))
                .build();
        mongodExecutable = mongodStarter.prepare(iMongoConfig);
        mongodExecutable.start();
    }

    @PreDestroy
    protected void stopEmbeddedMongoDb() {
        if (mongodExecutable != null)
            mongodExecutable.stop();
    }

}
