package com.daniel.galdao.service.embedded;

import com.daniel.galdao.model.CampanhaEvent;
import org.apache.qpid.server.Broker;
import org.apache.qpid.server.BrokerOptions;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.URI;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */
@Service
public class EmbeddedMessageBrokerService {

    private static final String BROKER_PORT = "10160";

    private Broker broker;
    private RabbitTemplate rabbitTemplate;

    /*
     * Configuração de um broker aninhado de mensagens.
     * Outras aplicaçoes poderiam se escrever nesse broker para receber notificações sobre os eventos.
     * Idealmente o broker não seria aninhado, e eu pessoalmente prefiro usar o Kafka e a integração do spring com Kafka, mas o QPID era o mais simples de aninhar.
     */
    @PostConstruct
    protected void startEmbeddedBroker() throws Exception {
        BrokerOptions brokerOptions = new BrokerOptions();
        brokerOptions.setConfigProperty("qpid.amqp_port", BROKER_PORT);
        brokerOptions.setConfigProperty("qpid.broker.defaultPreferenceStoreAttributes", "{\"type\": \"Noop\"}");
        brokerOptions.setConfigurationStoreType("Memory");
        brokerOptions.setStartupLoggedToSystemOut(false);
        broker = new Broker();
        broker.startup(brokerOptions);

        ConnectionFactory cf = new CachingConnectionFactory(new URI("amqp://guest:guest@localhost:10160/campanha"));
        RabbitAdmin admin = new RabbitAdmin(cf);

        TopicExchange exchange = new TopicExchange("event");
        admin.declareExchange(exchange);

        Queue queue = new Queue("event");
        admin.declareQueue(queue);

        admin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(""));

        rabbitTemplate = new RabbitTemplate(cf);
    }

    public void sendEventMessage(CampanhaEvent event) {
        try {
            rabbitTemplate.convertAndSend("event", "", event);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @PreDestroy
    protected void stopEmbeddedBroker() {
        broker.shutdown();
    }

}
