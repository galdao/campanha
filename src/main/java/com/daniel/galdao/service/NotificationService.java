package com.daniel.galdao.service;

import com.daniel.galdao.model.CampanhaEvent;
import com.daniel.galdao.repository.EventRepository;
import com.daniel.galdao.service.embedded.EmbeddedMessageBrokerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
@Service
public class NotificationService {

    @Autowired
    private EmbeddedMessageBrokerService messageBrokerService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private EventRepository eventRepository;

    @Async
    //Só o metodo chamado pelo CampanhaService deve ser assincrono, para evitar deletar eventos do mongo incorretamente
    public void notifyEvent(String type, Object data){
        notifyEvent(new CampanhaEvent(type, data));
    }

    //Servico de notificacao que envia eventos para o Broker de mensagens e para a API de notificacao da aplicacao de clientes
    //Caso haja uma falha nesse envio, o evento é salvo no mongo
    private void notifyEvent(CampanhaEvent event){
        try {
            messageBrokerService.sendEventMessage(event);
            customerService.notifyCampanhaEvent(event);
        } catch(Exception e){
            System.out.println("Failed to notify services, will try again later.");
            eventRepository.insert(event);
        }
    }

    //Pega os eventos que não foram enviados com sucesso do Mongo e tenta envialos novamente a cada 10 mins.
    @Scheduled(fixedRate = 600000)
    public void retrySendingEventNotifications() {
        List<CampanhaEvent> campanhaEventList = eventRepository.findAll();
        for (CampanhaEvent campanhaEvent : campanhaEventList) {
            try{
                notifyEvent(campanhaEvent);
                eventRepository.delete(campanhaEvent);
            } catch(Exception e){
                System.out.println("Failed to notify services, will try again later.");
            }
        }
    }

}
