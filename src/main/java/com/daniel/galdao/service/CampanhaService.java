package com.daniel.galdao.service;

import com.daniel.galdao.model.Campanha;
import com.daniel.galdao.repository.CampanhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */
@Service
public class CampanhaService {

    @Autowired
    private CampanhaRepository campanhaRepository;

    @Autowired
    private NotificationService notificationService;

    @Cacheable("campanha")
    public Campanha getCampanha(String id) {
        if (!campanhaRepository.exists(id)) {
            throw new IllegalArgumentException("Campanha com Id desejado não existe");
        }
        return campanhaRepository.findOne(id);
    }

    @Cacheable("campanhas")
    public List<Campanha> getCampanhas(String time) {
        if (time == null || time.isEmpty()) {
            return campanhaRepository.findByEndDateAfter(LocalDate.now().minusDays(1));
        } else {
            return campanhaRepository.findByTeamAndEndDateAfter(time, LocalDate.now().minusDays(1));
        }
    }

    @CacheEvict(cacheNames = {"campanha", "campanhas"}, allEntries = true)
    public String createCampanha(Campanha newCampanha) {
        //Ajusta data de campanhas que intersectam a data desta campanha, como no enunciado.
        adjustCampaignEndDates(newCampanha);
        newCampanha = campanhaRepository.insert(newCampanha);
        notificationService.notifyEvent("create", newCampanha);
        return newCampanha.getId();
    }

    @CacheEvict(cacheNames = {"campanha", "campanhas"}, allEntries = true)
    public void updateCampanha(Campanha updatedCampanha, String id) {
        if (!campanhaRepository.exists(id)) {
            throw new IllegalArgumentException("Campanha com Id desejado não existe");
        }
        Campanha currentCampanha = getCampanha(id);
        //Caso haja mudanca de datas as otras campanhas devem ter suas datas reajustadas novamente
        //Se a insercao desta campanha causou mudancas nas datas de outras, uma vez que sua data é mudada novamente, deveria haver rollback nos ajustes anteriores?
        if (!currentCampanha.getStartDate().equals(updatedCampanha.getStartDate()) || !currentCampanha.getEndDate().equals(updatedCampanha.getEndDate())){
            adjustCampaignEndDates(updatedCampanha);
        }
        updatedCampanha.setId(id);
        campanhaRepository.save(updatedCampanha);
        notificationService.notifyEvent("update", updatedCampanha);
    }

    @CacheEvict(cacheNames = {"campanha", "campanhas"}, allEntries = true)
    public void deleteCampanha(String id) {
        if (!campanhaRepository.exists(id)) {
            return;
        }
        campanhaRepository.delete(id);
        notificationService.notifyEvent("delete", id);
    }

    private void adjustCampaignEndDates(Campanha newCampanha){
        List<Campanha> intersectingCampanhas = campanhaRepository.findIntersectingCampanhhas(newCampanha.getStartDate(), newCampanha.getEndDate());
        if (!intersectingCampanhas.isEmpty()) {
            newCampanha.addDayToCampanha();
            intersectingCampanhas.forEach(Campanha::addDayToCampanha);
            while (true) {
                Optional<Campanha> matchingEndDateCampanha = intersectingCampanhas.stream()
                        .filter(c -> c.getEndDate().equals(newCampanha.getEndDate()))
                        .findAny();
                if (matchingEndDateCampanha.isPresent()) {
                    newCampanha.addDayToCampanha();
                } else {
                    campanhaRepository.save(intersectingCampanhas);
                    return;
                }
            }
        }
    }

}
