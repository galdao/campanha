package com.daniel.galdao.service;

import com.daniel.galdao.model.CampanhaEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by IFC.danielgaldao on 13/07/17.
 */
@Service
public class CustomerService {

    @Autowired
    private RestTemplate restTemplate;

    //Notifica a aplicacao de usuarios de que houve uma alteracao relacionada as campanhas
    public void notifyCampanhaEvent(CampanhaEvent event) {
        restTemplate.postForEntity("http://localhost:10200/event", event, String.class);
    }

}
