package com.daniel.galdao.controller;

import com.daniel.galdao.model.Campanha;
import com.daniel.galdao.service.CampanhaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by IFC.Daniel on 12/07/2017.
 */
@RestController
@RequestMapping("campanha")
public class CampanhaController {

    @Autowired
    private CampanhaService campanhaService;

    @GetMapping("{id}")
    public Campanha getCampanha(@PathVariable("id") String id){
        return campanhaService.getCampanha(id);
    }

    @GetMapping
    public List<Campanha> getCampanhas(@RequestParam(value = "time", required = false) String time){
        return campanhaService.getCampanhas(time);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity createCampanha(@RequestBody @Valid Campanha campanha,HttpServletRequest request) throws URISyntaxException {
        String id = campanhaService.createCampanha(campanha);
        URI location = new URI(String.format("%s/%s", request.getRequestURL().toString(), id));
        return ResponseEntity.created(location).build();
    }

    @PutMapping("{id}")
    public void updateCampanha(@RequestBody @Valid Campanha campanha, @PathVariable("id") String id){
        campanhaService.updateCampanha(campanha, id);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCampanha(@PathVariable("id") String id){
        campanhaService.deleteCampanha(id);
    }

}
